define([
    'jquery',
], function ($) {
    function gameView(categoriesData, controller) {
        this.finalStatesText = {
            lost: 'Game Over!',
            won: 'Game Won!'
        };

        this.controller = controller;
        this.categoriesData = categoriesData;
        this.appendCategories();
        this.handleOnChangeCat();
        this.handleOnSubmitLetter();
        this.buildGibbet();
        this.updateGameStats();
        this.handleOnStartOver();
    };
    /**
     * 
     * updates the games statistics 
     * 
     * @param {Object} gameStats
     * 
     */
    gameView.prototype.updateGameStats = function (gameStats) {
        var gameStats = gameStats || this.controller.getGameStats();

        $('#games_played').text(gameStats.games_played);
        $('#games_lost').text(gameStats.games_lost);
        $('#games_won').text(gameStats.games_won);
        $('#game_result').text(this.finalStatesText[gameStats.final_state]);
        //disable the input and the button
        this.toggleDisabled(true, ['#guess_letter', '#submit_letter']);
        this.toggleDisabled(false, ['#categories']);

        if (gameStats.final_state) {
            $('#word_to_guess').text(this.controller.getCurrentWord().name);
            this.toggleDisabled(true, ['#guess_letter', '#submit_letter', '#categories']);
        }
    };

    /**
     * 
     * adds or removes disabled prop on given elements by selector
     * 
     * @param {Boolean} doDisable
     * @param {String} elementsSelectors
    */
    gameView.prototype.toggleDisabled = function (doDisable, elementsSelectors) {
        for (var a = 0; a < elementsSelectors.length; a++) {
            var element = $(elementsSelectors[a]);
            if (!doDisable) {
                element.removeAttr('disabled');
            } else {
                element.attr('disabled', 'disabled');
            }
        }
    };

    /**
     * 
     * appends option elements to a select element
     * 
     * @param {Number} or {String} value
    */
    gameView.prototype.appendOptionEl = function (value) {
        var categoriesSelectEl = $('#categories');
        var element = document.createElement('option');
        element.text = value || '';
        element.value = value || '';
        categoriesSelectEl.append(element);
    };

    gameView.prototype.appendCategories = function () {
        this.appendOptionEl();
        for (var i in this.categoriesData.categories) {
            this.appendOptionEl(i);
        }
        //set default
        $('option[value=""]').prop('selected', true);
    };

    gameView.prototype.handleOnChangeCat = function () {
        //adds an event listener fo on change event on the select option element
        $('#categories').on('change', (function () {
            var val = $('#categories option:selected').text();
            //if the empty(default) option is selected, do nothing
            if (!val) {
                return;
            }
            this.toggleDisabled(false, ['#guess_letter', '#submit_letter', '#categories']);
            var selectedCat = val;
            var wordToGuess = this.controller.onChangeCat(selectedCat);
            $('#description').text(this.controller.getCurrentWord().description);
            $('#word_to_guess').text(wordToGuess);
        }).bind(this));
    };

    gameView.prototype.handleOnStartOver = function () {
        $('#start_again').on('click', (function () {
            //clear canvas drawing
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            //clear the word and the description fields
            $('#word_to_guess').text('');
            $('#description').text('');
            $('option[value=""]').prop('selected', true);
            $('#game_result').text('');
            this.controller.onStartAgain();
            this.buildGibbet();
        }).bind(this));
    };

    gameView.prototype.onWordMatch = function (updatedWord) {
        $('#word_to_guess').text(updatedWord);
    };

    gameView.prototype.onNoWordMatch = function () {
        switch (this.controller.getState('errors')) {
            case 5:
                this.drawRightFoot();
            case 4:
                this.drawLeftFoot();
            case 3:
                this.drawRightHand();
            case 2:
                this.drawLeftHand();
            case 1:
                this.drawHead();
        }
    };

    gameView.prototype.handleOnSubmitLetter = function () {
        $('#submit_letter').on('click', (function () {
            var letter = $('#guess_letter').val();
            var matchResult = this.controller.onSubmitLetter(letter);
            if (matchResult.hasMatching) {
                this.onWordMatch(matchResult.updatedWord);
            } else {
                this.onNoWordMatch();
            }
            $('#guess_letter').val('');
            this.controller.checkState();
        }).bind(this));
    };

    gameView.prototype.buildGibbet = function () {
        this.canvas = document.getElementById('canvas');
        this.ctx = canvas.getContext('2d');
        this.drawGibbet();
    };

    gameView.prototype.drawGibbet = function () {
        this.ctx.beginPath();
        this.ctx.strokeStyle = 'blue';

        this.ctx.moveTo(20, 230);
        this.ctx.lineTo(20, 10);
        this.ctx.lineTo(100, 10);
        this.ctx.lineTo(100, 20);
        this.ctx.stroke();
    };

    gameView.prototype.drawHead = function () {
        this.ctx.beginPath();
        this.ctx.arc(100, 40, 20, 0, 2 * Math.PI);
        this.ctx.stroke();
        this.drawBody();
    };

    gameView.prototype.drawBody = function () {
        this.ctx.beginPath();
        this.ctx.moveTo(100, 60);
        this.ctx.lineTo(100, 120);
        this.ctx.stroke();
    };

    gameView.prototype.drawLeftHand = function () {
        this.ctx.beginPath();
        this.ctx.moveTo(100, 80);
        this.ctx.lineTo(50, 60);
        this.ctx.stroke();
    };


    gameView.prototype.drawRightHand = function () {
        this.ctx.beginPath();
        this.ctx.moveTo(100, 80);
        this.ctx.lineTo(150, 60);
        this.ctx.stroke();
    };

    gameView.prototype.drawLeftFoot = function () {
        this.ctx.beginPath();
        this.ctx.moveTo(100, 120);
        this.ctx.lineTo(70, 180);
        this.ctx.stroke();
    };

    gameView.prototype.drawRightFoot = function () {
        this.ctx.beginPath();
        this.ctx.moveTo(100, 120);
        this.ctx.lineTo(130, 180);
        this.ctx.stroke();
    };

    return gameView;

});