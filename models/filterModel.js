define([
    'jquery',
    'models/catsDataModel',
    'models/userGameStateModel'
], function ($, dataModel, userGameStateModel) {
    function filterModel() {
        this.data = dataModel.getData();
        this.randomWord = "";
        this.wordToGuessObj = [];
        this.wordToGuessMap = [];
    };

    filterModel.prototype.getMap = function () {
        return this.wordToGuessMap;
    };

    filterModel.prototype.getCurrentWord = function () {
        return this.randomWord;
    };

    filterModel.prototype.reset = function () {
        this.wordToGuessObj = [];
        this.wordToGuessMap = [];
        this.description = "";
    };

    filterModel.prototype.getRandNum = function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    filterModel.prototype.getRandomWord = function (selectedCategory) {
        var maxPossibleIndex = this.data.categories[selectedCategory].length;
        var wordIndex = this.getRandNum(0, maxPossibleIndex);
        this.randomWord = this.data.categories[selectedCategory][wordIndex];

        return this.randomWord;
    };
    
    /**
     * This method allows you to format the word by the map
     * 
     * @param {String} selectedCat
     * @param {Array} map
     * 
     * @returns {String} - the formatedWord
     */
    filterModel.prototype.formatWordByMap = function (selectedCat, map) {
        
        //if there's no map, get a random word and store it as a object
        if (!map) {
            var randomWord = this.getRandomWord(selectedCat);
            this.wordToGuessObj = randomWord.name.split("");
        }
        this.wordToGuessMap = map || this.getWordMap(randomWord.name);

        var formatedWord = "";

        for (var c = 0; c < this.wordToGuessMap.length; c++) {
            if (this.wordToGuessMap[c]) {
                formatedWord += this.wordToGuessObj[c];
            } else {
                formatedWord += ' _ ';
            }
        }

        return formatedWord;
    };


    /**
     * This method generates the map for the word
     * 
     * @param {String} word
     *  
     * @returns {Array} - the map   
    */
    filterModel.prototype.getWordMap = function (word) {
        //if the word is made of more than one word
        //get all words
        var words = word.split(" ");
        var wordObj = word.split("");
        var map = [];
        //iterate trough all words 
        for (var a = 0; a < words.length; a++) {
            //set the first element to true
            if (a !== 0) {
                map.push(1);
            }
            for (var b = 0; b < words[a].length; b++) {
                if (b == 0 || b == words[a].length - 1) {
                    //set the first and the last element to true
                    map.push(1);
                } else {
                    map.push(0)
                }
            }
        }

        return map;
    };
    
    /**
     * 
     * this method checks if the word that the customer has entered matches the current word
     * 
     * @param {String} word
     * 
     * @returns {Boolean} 
    */
    filterModel.prototype.checkWord = function (word) {
        if (word.toLowerCase() === this.randomWord.name.toLowerCase()) {
            for (var k = 1; k < this.wordToGuessMap.length - 1; k++) {
                this.wordToGuessMap[k] = 1;
            }
            return true;
        } else {
            userGameStateModel.set('errors', 5);
            return false;
        }
    };


    /**
     * 
     * checks if the current word contains the entered letter 
     * 
     * @param {String} letter
     * 
     * @returns {Object} - indicator for match and the updated word
    */
    filterModel.prototype.hasMatching = function (letter) {
        var hasMatching = false;
        var wordToGuess = '';

        if (letter.length !== 1) {
            hasMatching = this.checkWord(letter);

        } else {
            for (var i = 0; i < this.wordToGuessObj.length; i++) {
                if (letter == this.wordToGuessObj[i]) {
                    hasMatching = true;
                    this.wordToGuessMap[i] = 1; //update the map
                }
            }
            if (hasMatching) {
                //update the word to show the alredy guessed letters
                wordToGuess = this.formatWordByMap(null, this.wordToGuessMap);
            } else {
                //increase the number of errors
                userGameStateModel.set('errors', userGameStateModel.get('errors') + 1);
            }
        }


        return {
            hasMatching: hasMatching,
            updatedWord: wordToGuess
        };
    };

    return new filterModel();
});