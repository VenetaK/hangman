define(['jquery'], function($) {
    function userGameStateModel(){
        this.errors = 0;
        this.games = 0;
        this.games_won = 0;
        this.games_lost = 0;
        this.final_state = '';
    };
    
    userGameStateModel.prototype.get = function (propName) {
        return this[propName];
    };
    
    userGameStateModel.prototype.set = function (propName, value) {
        this[propName] = value;
    }
    
    userGameStateModel.prototype.checkState = function (map) {
        if (this.get('errors') === 5) {
            this.set('games', this.get('games')+1);
            this.set('games_lost', this.get('games_lost')+1);
            
            $(this).trigger('final-state', {state: 'lost'});
        }
         
        if(map.indexOf(0) === -1){
            this.set('games', this.get('games')+1);
            this.set('games_won', this.get('games_won')+1);
            
            $(this).trigger('final-state', {state: 'won'});
        }
    }
    
    return new userGameStateModel();
});