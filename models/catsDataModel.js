define([], function() {
    function dataModel() {
        this.data = {
            categories: {
                'books': [
                    {
                        name: 'The Lord of The Rings',
                        description: 'A best seller written by J.R.R.Tolkien'
                    },
                    {
                        name: 'Inferno',
                        description: "One of Dan Brown's most read books"
                    },
                    {
                        name: "Hobbit",
                        description: "A story about dwarfs, elves, a dragon and a...?"
                    }
                ],
                'video games': [
                    {
                        name: 'World of Warcraft',
                        description: "The best MMORPG EVER!!!"
                    },
                    {
                        name: 'Counter Strike',
                        description: "First Person Shooter"
                    }
                ],
                'cities': [
                    {
                        name: 'Sofia',
                        description: 'The capital city of Bulgaria'
                    },
                    {
                        name: 'London',
                        description: 'A city next to a river in England'
                    },
                    {
                        name: 'Varna',
                        description: 'A city on the Bulgarian cost of the Black sea'
                    }
                ],
            }
        };
        this.getData = function(){
            return this.data;
        };
    };
        
    return new dataModel();
});