define([
    'jquery',
    'models/catsDataModel',
    'models/filterModel',
    'models/userGameStateModel',
    'views/canvasView'
], function($, catsDataModel, filterModel, userGameStateModel, gameView) {
    function gameController () {
        this.view = new gameView(catsDataModel.getData(), this);
        this.onGameFinalState();
    };
    
    gameController.prototype.getGameStats = function () {
        return {
            games_played: userGameStateModel.get('games'),
            games_won: userGameStateModel.get('games_won'),
            games_lost: userGameStateModel.get('games_lost'),
            final_state: userGameStateModel.get('final_state')
        }; 
    };
    
    gameController.prototype.onChangeCat = function(selectedCat){
        //returns randomlly chosen and formated word to be guessed
        return filterModel.formatWordByMap(selectedCat);
    };
        
    gameController.prototype.onSubmitLetter = function(letter){
        return filterModel.hasMatching(letter);
    };
    
    gameController.prototype.getState = function (propName) {
        return userGameStateModel.get(propName);
    };
    
    gameController.prototype.onGameFinalState = function () {
        $(userGameStateModel).on('final-state', (function(e, options){
            filterModel.reset();
            userGameStateModel.set('errors', 0);    
            userGameStateModel.set('final_state', options.state);
            this.view.updateGameStats();
        }).bind(this));
    };

    gameController.prototype.onStartAgain = function () {
         filterModel.reset();
            userGameStateModel.set('errors', 0);
            
            userGameStateModel.set('final_state', '');
            this.view.updateGameStats(this.getGameStats());
    };
    
    gameController.prototype.getCurrentWord = function () {
        return filterModel.getCurrentWord();
    };
    
    gameController.prototype.checkState = function () {
        userGameStateModel.checkState(filterModel.getMap());
    };
    return gameController; 
}); 